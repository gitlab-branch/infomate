@extends('layouts.app')

@section('content')
<script>
    $( function() {
      $( "#slider-range" ).slider({
        range: true,
        min: 50000,
        max: 1000000,
        values: [ 100000, 300000 ],
        slide: function( event, ui ) {
          $( "#amount" ).val(ui.values[ 0 ] + ui.values[ 1 ] );
        }
      });
      $( "#amount" ).val( $( "#slider-range" ).slider( "values", 0 ) +
        $( "#slider-range" ).slider( "values", 1 ) );
    } );
    </script>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Add Patner Preference') }}</div>

                <div class="card-body">
                    
                    <form method="POST" action="{{ route('patner.store') }}">
                        @csrf

                        <div class="row mb-3">
                        <label for="amount">Income range:</label>
                        <input type="text" id="amount" name="range" value="" class="form-control"  readonly style="border:0; color:#f6931f; font-weight:bold;">
                        </p>
                        
                        <div id="slider-range" ></div>
                        </div>
                        <div class="row mb-3">
                            <label for="occupation" class="col-md-4 col-form-label text-md-end">{{ __('occupation') }}</label>

                            <div class="col-md-6">
                                <select id="occupation" multiple="multiple"  class="form-control @error('occupation')  is-invalid @enderror" name="occupation[]" required >
                                    <option value="private">Private</option>
                                    <option value="government">Government</option>
                                    <option value="business">Business</option>

                                </select>
                                
                            </div>
                            
                        </div>
                        <div class="row mb-3">
                            <label for="family" class="col-md-4 col-form-label text-md-end">{{ __('family') }}</label>

                            <div class="col-md-6">
                                <select id="family" multiple="multiple"  class="form-control @error('family')  is-invalid @enderror" name="family[]" required >
                                    <option value="joint">Joint</option>
                                    <option value="nuclear">Nuclear</option>
                                    

                                </select>
                                
                            </div>
                            
                        </div>
                        <div class="row mb-3">
                            <label for="manglik" class="col-md-4 col-form-label text-md-end">{{ __('manglik') }}</label>

                            <div class="col-md-6">
                                <select id="manglik"  class="form-control @error('manglik')  is-invalid @enderror" name="manglik" required >
                                    <option value="yes">Yes</option>
                                    <option value="no">No</option>
                                    <option value="both">Both</option>

                                </select>
                                
                            </div>
                            
                        </div>

                        

                        <div class="row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('ADD') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection