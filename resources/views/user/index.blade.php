@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Suggestions') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if(isset($data))
                    {{ __('index') }}
                    <div class="card-body">
                        <table class="table">
                          <thead>
                            <tr>
                              <th scope="col">Name</th>
                              <th scope="col">Income</th>
                              <th scope="col">Family</th>
                              <th scope="col">Manglik</th>
                              <th scope="col">Job Type</th>
                            </tr>
                          </thead>
                          <tbody>
                            @if(isset($data))
                            @foreach ($data as $item)
                            <tr>
                                <th scope="row">{{$item->fname }} {{$item->lname }}</th>
                                <td>{{$item->income }}</td>
                                <td>{{$item->family }}</td>
                                <td>{{$item->manglik }}</td>
                                <td>{{$item->occupation }}</td>
                              </tr>
                            @endforeach
                           @endif
                          </tbody>
                        </table>
                      </div>
                    @else
                    <div class="card-body">
                      <H1>Please Complete your Preference for your Patner Detail <a class="btn btn-warning" href="{{route('patner.index')}}">Complete</a> </H1>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection