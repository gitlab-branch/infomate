<?php

namespace Database\Seeders;

use App\Models\User;
use Faker\Factory as Faker;
//use Faker\Generator as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        foreach(range(1,10) as $index)
        {
            $gender=$faker->randomElements(['male', 'female'])[0];
        $user= new User();
        $user->fname=$faker->name($gender);
        $user->lname=$faker->lastName;
        $user->email=$faker->email;
        $user->dob=$faker->date;
        $user->gender=$gender;
        $user->occupation=$faker->randomElements(['private', 'government','business'])[0];
        $user->family=$faker->randomElements(['joint', 'nuclear'])[0];
        $user->manglik=$faker->randomElements(['yes', 'no'])[0];
        $user->income=$faker->numberBetween($min = 100000, $max = 900000);
        $user->password=Hash::make('1234');
        $user->save();
        }

    }
}
