<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('fname');
            $table->string('lname');
            $table->string('email')->unique();
            $table->date('dob')->nullable();
            $table->bigInteger('income')->nullable();
            $table->enum('gender', ['male', 'female'])->nullable();
            $table->enum('occupation', ['private', 'government','business'])->nullable();
            $table->enum('family', ['joint', 'nuclear'])->nullable();
            $table->enum('manglik', ['yes', 'no'])->nullable();
            $table->string('log_type')->default('email')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
