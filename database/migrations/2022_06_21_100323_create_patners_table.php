<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patners', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('min_income');
            $table->integer('max_income');
            $table->enum('job_private', ['private'])->default(null)->nullable();
            $table->enum('job_government', ['government'])->default(null)->nullable();
            $table->enum('job_business', ['business'])->default(null)->nullable();
            $table->enum('manglik_yes', ['yes'])->default(null)->nullable();
            $table->enum('manglik_no', ['no'])->default(null)->nullable();
            $table->enum('family_joint', ['joint'])->default(null)->nullable();
            $table->enum('family_nuclear', ['nuclear'])->default(null)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patners');
    }
}
