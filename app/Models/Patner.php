<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Patner extends Model
{
    use HasFactory;
    
    protected $fillable=['user_id',
    'min_income',
    'max_income',
    'job_private',
    'job_government',
    'job_business',
    'manglik_yes',
    'manglik_no',
    'family_joint',
    'family_nuclear',];
}
