<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class EnsureUserDataIsComplete
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        //$matchThese = ['dob' => NULL, 'gender' => NULL, 'family'=>NULL];
        $id=Auth::user()->id;
        if (Auth::user()->dob ==NULL || Auth::user()->gender ==NULL || Auth::user()->income ==NULL || Auth::user()->family ==NULL || Auth::user()->occupation ==NULL) {
            return Redirect::route('edit', ['id'=>$id]);
        }
        //echo Auth::user()->dob;
        return $next($request);
    }
}
