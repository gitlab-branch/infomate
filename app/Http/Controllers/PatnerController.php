<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Patner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PatnerController extends Controller
{
    public function index(){
        
        // $user=User::where('email','indiacrypto653@gmail.com')->first();
        // dd($user->id);
        return view('user.patner');
    }

    public function store(Request $request){
        $income=explode('-',$request->range);
        if(count($income)>=1){
            $income_min=0;
            $income_max=intval($income[0]);
        }else{
            $income_min=intval($income[0]);
            $income_max=intval($income[1]);
        }
        //$income_min=$income[0];
        $job=['private'=>null,'government'=>null,'business'=>null];
        $manglik=['yes'=>null,'no'=>null];
        $family=['joint'=>null,'nuclear'=>null];
        foreach($request->occupation as $k=>$hi){
               $job[$hi]=$hi; 
        }
        foreach($request->family as $k=>$hi){
            $family[$hi]=$hi; 
        }
        if ($request->manglik=='yes') {
           $manglik['yes']='yes';
        } elseif($request->manglik=='no') {
            $manglik['no']=null;
        }else{
            $manglik['yes']='yes';
            $manglik['no']='no';
        }
         $new_patner= new Patner();
        $new_patner->user_id=Auth::user()->id;
        $new_patner->min_income=$income_min;
        $new_patner->max_income=$income_max;
        $new_patner->job_private=$job['private'];
        $new_patner->job_government=$job['government'];
        $new_patner->job_business=$job['business'];
        $new_patner->manglik_yes=$manglik['yes'];
        $new_patner->manglik_no=$manglik['no'];
        $new_patner->family_joint=$family['joint'];
        $new_patner->family_nuclear=$family['nuclear'];
        $new_patner->save();
        
        //dd($income);
       return redirect('/');
    }
}
