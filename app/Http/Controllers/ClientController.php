<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Patner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ClientController extends Controller
{
    public function index(){
        $patner=Patner::where('user_id',Auth::user()->id)->first();
        if($patner){

        //dd(Auth::user()->gender);
        $gender='';
        if(Auth::user()->gender=='male'){
            $gender='female';
        }else{
            $gender='male';
        }
        $income_min=$patner->min_income;
        $income_max=$patner->max_income;
        //$gender=$gender;
        $manglik_yes=$patner->manglik_yes;
        $manglik_no=$patner->manglik_no;
        $family_joint=$patner->family_joint;
        $family_nuclear=$patner->family_nuclear;
        $job_private=$patner->job_private;
        $job_government=$patner->job_government;
        $job_business=$patner->job_business;
        // $data1=User::when($income, function ($q) use ($income) {
        //     return $q->where('income', '>' ,$income);
        // })
        // ->when($gender, function ($q) use ($gender) {
        //     return $q->where('gender', $gender);
        // })
        // ->when($manglik, function ($q) use ($manglik) {
        //     return $q->where('manglik', $manglik)->orWhere('manglik', 'no');
        // })
        
        // ->when($family, function ($q) use ($family) {
        //     return $q->where('family', $family);
        // })
        // ->when($job, function ($q) use ($job) {
        //     return $q->where('occupation', '=', $job,);
        // })
        // ->get();
        $data= User::whereBetween('income',[$income_min,$income_max])
        //->addSelectRaw('(78+87*2) AS TotalFee')
        ->where(function($query) use ($job_private,$job_government,$job_business){
            $query->where('occupation', '=', $job_private)
        ->orWhere('occupation', '=', $job_government)->orWhere('occupation', '=', $job_business);
        })
        ->where(function($query) use ($manglik_yes,$manglik_no){
            $query->where('manglik', '=', $manglik_yes)
        ->orWhere('manglik', '=', $manglik_no);})
        ->where(function($query) use ($family_joint,$family_nuclear){
            $query->where('family', '=', $family_joint)
        ->orWhere('family', '=', $family_nuclear);})
        ->where('gender','=',$gender)
        
        
        //->toSql();
       ->get();
    //     echo "<pre>";
    //    print_r($data);
    //return json_encode($data);
        return view('user.index',compact('data'));
        }else{
            
            return view('user.index');
        }
    }

    public function edit($id){
        $user= Auth::user();
        //dd($user);
        return view('user.edit',compact('user'));
    }
    public function update(Request $request){
        $user= Auth::user()->id;
        //dd($request);
        $this->validate($request, [
            'fname' => ['required', 'string', 'max:255'],
            'lname' => ['required', 'string', 'max:255'],
            
            'dob' => ['required', 'date'],
            'gender' => ['required', 'string'],
            
            'income'=>['required','numeric'],
            'occupation'=>['required','string'],
            'family'=>['required','string'],
            'manglik'=>['required','string']
        ]);

        $affectedRows = User::where("id", Auth::user()->id)
                            ->update(["dob" => $request->dob,
                                "gender" => $request->gender,
                                "income" => $request->income,
                                "occupation" => $request->occupation,
                                "manglik" => $request->manglik,
                                "family" => $request->family,

                            ]);
       // dd($user);
        return view('user.index');
    }
}
