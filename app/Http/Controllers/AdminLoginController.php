<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class AdminLoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::ADMINHOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }

    public function showLoginForm()
    {
        return view('admin.login');
    }


    public function login(Request $request)
    {
        //dd($request);
        $this->validateLogin($request);
        
        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        return $this->sendFailedLoginResponse($request);
    }

    protected function credentials(Request $request)
    {
        //$admin = Admin::where('email',$request->email)->first();
        // if (count($admin)) {
        //     if ($admin->status == 0) {
        //         return ['email'=>'inactive','password'=>'You are not an active person, please contact Admin'];
        //     }else{
        //         return ['email'=>$request->email,'password'=>$request->password,'status'=>1];
        //     }
        // }
        return $request->only($this->username(), 'password');
    }

    

    protected function guard()
    {
        return Auth::guard('admin');
    }

    public function logout( Request $request )
{
    if(Auth::guard('admin')->check()) // this means that the admin was logged in.
    {
        Auth::guard('admin')->logout();
        return redirect()->route('admin.login');
    }

    $this->guard()->logout();
    $request->session()->invalidate();

    return $this->loggedOut($request) ?: redirect('/');
}
}
