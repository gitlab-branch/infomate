<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;

class GoogleController extends Controller
{
    public function googleRedirect(){
        return Socialite::driver('google')->redirect();
    }

    public function googleCallback(){
        $userData = Socialite::driver('google')->stateless()->user();
        
        //dd($userData);
        if($user=User::where('email',$userData->email)->first()){
           // dd($user->lname);
            Auth::loginUsingId($user->id);
        }else{
        $user = User::updateOrCreate([
            'email' =>$userData->email,
            'fname' =>$userData->user['given_name'],
            'lname' =>$userData->user['given_name'],
            'password' => Hash::make($userData['id'])
        ]);
        Auth::loginUsingId($user->id);
    }
        return redirect('home');
        //dd($user->email);
    }
}
