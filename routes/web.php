<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Laravel\Socialite\Facades\Socialite;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/auth/googleredirect', [App\Http\Controllers\GoogleController::class,'googleRedirect'])->name('googleRedirect');
Route::get('/auth/googlecallback', [App\Http\Controllers\GoogleController::class,'googleCallback']);
 
Route::get('/auth/callback', function () {
    $user = Socialite::driver('google')->user();
    dd($user);
    // $user->token
});

Auth::routes();
Route::post('/admin/login', [App\Http\Controllers\AdminLoginController::class, 'login'])->name('admin.login');
Route::get('/admin/login', [App\Http\Controllers\AdminLoginController::class, 'showLoginForm'])->name('admin');
Route::middleware(['auth:admin'])->group(function(){
    
    Route::get('/admin/home', [App\Http\Controllers\AdminController::class, 'index'])->name('admin.home');
    
    Route::post('/admin/logout', [App\Http\Controllers\AdminLoginController::class, 'logout'])->name('admin.logout');
});
Route::get('/admin/login', [App\Http\Controllers\AdminLoginController::class, 'showLoginForm'])->name('admin');
Route::get('/admin/home', [App\Http\Controllers\AdminController::class, 'index'])->name('admin.home');
Route::post('/admin/login', [App\Http\Controllers\AdminLoginController::class, 'login'])->name('admin.login');
Route::post('/admin/logout', [App\Http\Controllers\AdminLoginController::class, 'logout'])->name('admin.logout');
Route::middleware(['auth','usercomplete'])->group(function () {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/', [App\Http\Controllers\ClientController::class, 'index'])->name('index');

Route::get('/patner', [App\Http\Controllers\PatnerController::class, 'index'])->name('patner.index');
Route::post('/patner', [App\Http\Controllers\PatnerController::class, 'store'])->name('patner.store');
});
Route::get('/edit/{id}', [App\Http\Controllers\ClientController::class, 'edit'])->middleware('auth')->name('edit');
Route::post('/update', [App\Http\Controllers\ClientController::class, 'update'])->middleware('auth')->name('update');


